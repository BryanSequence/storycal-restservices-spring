package ph.sprk.storycal.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import ph.sprk.storycal.app.config.JwtFilter;



@SpringBootApplication
public class StorycalRestservicesSpringApplication extends SpringBootServletInitializer {
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Bean
	public FilterRegistrationBean jwtFilter() {
		final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(new JwtFilter());
		registrationBean.addUrlPatterns("/api/*");

		return registrationBean;
	}

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(StorycalRestservicesSpringApplication.class);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(StorycalRestservicesSpringApplication.class, args);
    }

}
