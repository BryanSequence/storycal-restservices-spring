package ph.sprk.storycal.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ph.sprk.storycal.app.repositories.services.CategoryRefService;

@RestController
@RequestMapping("/api")
public class CategoryController {
	
	@Autowired
	private CategoryRefService catService;
	
	@RequestMapping(path = "/category/list/", method = RequestMethod.GET)
	public ResponseEntity<?> registerAccount() throws Exception {
		return new ResponseEntity<Object>(catService.findAll(), HttpStatus.OK);
	}
}
