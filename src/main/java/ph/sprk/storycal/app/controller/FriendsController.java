package ph.sprk.storycal.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ph.sprk.storycal.app.entities.Friendship;
import ph.sprk.storycal.app.repositories.dao.FriendshipDAO;
import ph.sprk.storycal.app.repositories.services.FriendshipService;
import ph.sprk.storycal.app.utils.GenericResponse;

@RestController
@RequestMapping("/api")
public class FriendsController {
	
	@Autowired
	private FriendshipService friendsService;
	
	@Autowired
	private FriendshipDAO fDAO;
	
	@RequestMapping(value = "/follow", method = RequestMethod.POST)
	public ResponseEntity<?> follow(@RequestBody Friendship friend) {
		return new ResponseEntity<>(friendsService.save(friend), HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/followers/{following_id}", method = RequestMethod.GET)
	public ResponseEntity<?> followers(@PathVariable int following_id) throws Exception {
		return new ResponseEntity<>(fDAO.getFollowers(following_id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/following/{user_id}", method = RequestMethod.GET)
	public ResponseEntity<?> following(@PathVariable int user_id) throws Exception {
		return new ResponseEntity<>(fDAO.getFollowing(user_id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/unfollow/{user_id}/following/{following_id}", method = RequestMethod.POST)
	public ResponseEntity<?> unlikeStory(@PathVariable int user_id, @PathVariable int following_id) throws Exception {
		
		Friendship l = friendsService.findByUserIdFollowing(user_id, following_id);
		
		if (l == null) 
			return new ResponseEntity<>(new GenericResponse(false, "user cannot be found"), HttpStatus.BAD_REQUEST);
		
		int follow_id = l.getFollowId();
		friendsService.deleteFollow(follow_id);
		
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
}
