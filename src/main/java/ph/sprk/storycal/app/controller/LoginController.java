package ph.sprk.storycal.app.controller;

import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import ph.sprk.storycal.app.entities.User;
import ph.sprk.storycal.app.entities.UserAccount;
import ph.sprk.storycal.app.repositories.services.UserAccountService;
import ph.sprk.storycal.app.repositories.services.UserService;
import ph.sprk.storycal.app.utils.GenericResponse;
import ph.sprk.storycal.app.utils.PasswordUtils;


@CrossOrigin(origins = "http://localhost", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class LoginController {
	
	@Autowired
	private UserAccountService userAccountService;
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<Object> login(@RequestBody UserAccount login) throws Exception {
		PasswordUtils pw = new PasswordUtils();
		String jwtToken = "";

		if (login.getUsername() == null || login.getPassword() == null) {
			throw new ServletException("Please fill in username and password");
		}

		String username = login.getUsername();
		String password = login.getPassword();

		UserAccount userAccount = userAccountService.findByUsername(username);

		if (userAccount == null) {
			throw new ServletException("User email not found.");
		}

		String pwd = userAccount.getPassword();

		if (!pw.check(password, pwd)) {
			throw new ServletException("Invalid login. Please check your name and password.");
		}

		jwtToken = Jwts.builder().setSubject(username).claim("roles", "user").setIssuedAt(new Date())
				.signWith(SignatureAlgorithm.HS256, "secretkey").compact();
		
			HashMap<String, Object> r = new HashMap<String, Object>();
			User user = userService.findByUserSecId(userAccount.getUser_sec_id());

			r.put("userId", user.getUserId());
			r.put("fuid", user.getFuid());
			r.put("name", user.getName());
			r.put("picture_url", user.getPicture_url());
			r.put("cover_url", user.getCover_photo_url());
			r.put("username", userAccount.getUsername());
			r.put("accountStatus", userAccount.getAccount_status());
			
			
			GenericResponse response = new GenericResponse();
			
			response.setKeyValuePair("success", true);
			response.setKeyValuePair("message", "Successfully Logged-In!");
			response.setKeyValuePair("authToken", jwtToken);
			response.setKeyValuePair("userData", r);

		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}
}
