package ph.sprk.storycal.app.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ph.sprk.storycal.app.controller.services.SignUpServicesImpl;
import ph.sprk.storycal.app.entities.User;
import ph.sprk.storycal.app.mapper.ChangePassword;
import ph.sprk.storycal.app.repositories.dao.ProfileDAO;
import ph.sprk.storycal.app.repositories.dao.UserDAO;
import ph.sprk.storycal.app.repositories.services.UserService;
import ph.sprk.storycal.app.utils.PasswordUtils;



@RestController
@RequestMapping("/api")
public class ProfileController {
	
	@Autowired
	SignUpServicesImpl signupServices;
	
	@Autowired
	private UserService service;
	
	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private ProfileDAO profileDAO;
	
	@RequestMapping(path = "/update-profile/{user_id}", method = RequestMethod.POST)
	public ResponseEntity<?> registerAccount(@PathVariable int user_id, @RequestBody User u) throws Exception {
		return new ResponseEntity<Object>(signupServices.updateUserProfile(user_id, u), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/get-profile-summary/{user_id}", method = RequestMethod.GET)
	public ResponseEntity<?> getProfileSummary(@PathVariable int user_id) throws Exception {
		return new ResponseEntity<>(userDAO.getProfileSummary(user_id), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/view-profile/user/{user_id}/profile/{profile_id}", method = RequestMethod.GET)
	public ResponseEntity<?> viewProfile(@PathVariable int user_id, @PathVariable int profile_id) throws Exception {
		return new ResponseEntity<>(userDAO.viewProfile(user_id, profile_id), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/update-profile-password/{user_id}", method = RequestMethod.POST)
	public ResponseEntity<?> updatePassword(@PathVariable int user_id, @RequestBody ChangePassword cp) throws Exception {
		PasswordUtils pw = new PasswordUtils();

		User u = service.findByUserId(user_id);
		
		if (u == null) 
			return new ResponseEntity<>("cannot found user", HttpStatus.BAD_REQUEST);
		
		int sec_id = u.getUserAccount().getUser_sec_id();
		System.out.println("OLD PASSWORD" + u.getUserAccount().getPassword());
		if (!pw.check(cp.getOldPassword(), u.getUserAccount().getPassword())) {
			//throw new ServletException("Current password is wrong. Please check current password.");
			return new ResponseEntity<>("Current password is wrong. Please check current password.", HttpStatus.BAD_REQUEST);
		}
		
		String hashed_password = pw.getSaltedHash(cp.getNewPassword());
		profileDAO.updatePassword(sec_id, hashed_password);
		return new ResponseEntity<>("Password Successfully Updated", HttpStatus.OK);
	}
	
	
}
