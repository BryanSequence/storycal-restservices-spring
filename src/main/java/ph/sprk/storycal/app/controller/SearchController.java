package ph.sprk.storycal.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.sprk.storycal.app.repositories.services.UserService;

@RestController
@RequestMapping("/api")
public class SearchController {
	
	@Autowired
	private UserService service;
	
	@RequestMapping(path = "/search", method = RequestMethod.POST)
	public ResponseEntity<?> registerAccount(@RequestParam("name") String name) throws Exception {
		return new ResponseEntity<Object>(service.findByName(name), HttpStatus.OK);
	}
}
