package ph.sprk.storycal.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ph.sprk.storycal.app.controller.services.SignUpServicesImpl;
import ph.sprk.storycal.app.entities.User;
import ph.sprk.storycal.app.entities.Verification;
import ph.sprk.storycal.app.mapper.AccountRequest;
import ph.sprk.storycal.app.utils.GenericResponse;


@RestController
@RequestMapping("/signup")
public class SignUpController {
	
	@Autowired
	SignUpServicesImpl signupServices;
	
	@RequestMapping(path = "/verify", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<?> verifyEmail(@RequestBody User u) throws Exception {
		String email = u.getEmail();
		
		if (email.trim().equals("") || email == null) 
			return new ResponseEntity<Object>(new GenericResponse(false,"Email address should not be empty!"), HttpStatus.BAD_REQUEST);
		
		return new ResponseEntity<Object>(signupServices.verifyEmailAddress(email), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/validate", method = RequestMethod.POST)
	public ResponseEntity<?> validateCode(@RequestBody Verification vcode) throws Exception {
		
		String email = vcode.getEmail();
		String code  = vcode.getVerification_code();
		
		if (code == null || email == null) 
			return new ResponseEntity<Object>(new GenericResponse(false,"Verification Code and Email Address should not be empty!"), HttpStatus.BAD_REQUEST);
		else if (code.trim().equals("") || email.trim().equals("")) 
			return new ResponseEntity<Object>(new GenericResponse(false,"Verification Code and Email Address should not be empty!"), HttpStatus.BAD_REQUEST);

		return new ResponseEntity<Object>(signupServices.validateVerificationCode(email, code) , HttpStatus.OK);
	}
	
	@RequestMapping(path = "/register", method = RequestMethod.POST)
	public ResponseEntity<Object> registerAccount(@RequestBody AccountRequest req) throws Exception {
		String email = req.getUser().getEmail();
		String username = req.getUserAccount().getUsername();
		String password = req.getUserAccount().getPassword();
		
		if (email == null || username == null || password == null) 
			return new ResponseEntity<Object>(new GenericResponse(false,"Email, Username and Password should not be empty!"), HttpStatus.BAD_REQUEST);
		else if (username.trim().equals("") || password.trim().equals("") || email.trim().equals("")) 
			return new ResponseEntity<Object>(new GenericResponse(false,"Email, Username and Password should not be empty!"), HttpStatus.BAD_REQUEST);

		return new ResponseEntity<Object>(signupServices.createUserAccount(email, username, password), HttpStatus.OK);
	}
	
	
	@RequestMapping(path = "/check-username/{username}", method = RequestMethod.GET)
	public ResponseEntity<?> checkUserName(@PathVariable String username) throws Exception {
		if (signupServices.isUserNameExist(username)) 
			return new ResponseEntity<Object>(new GenericResponse(false, "The username already taken"), HttpStatus.BAD_REQUEST);

		return new ResponseEntity<Object>(new GenericResponse(true, "The username is available"), HttpStatus.OK);
	}

}
