package ph.sprk.storycal.app.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.sprk.storycal.app.entities.StoryComments;
import ph.sprk.storycal.app.entities.StoryLikes;
import ph.sprk.storycal.app.entities.User;
import ph.sprk.storycal.app.entities.UserStory;
import ph.sprk.storycal.app.repositories.dao.NewsfeedDAO;
import ph.sprk.storycal.app.repositories.dao.StoryDAO;
import ph.sprk.storycal.app.repositories.services.StoryCommentsService;
import ph.sprk.storycal.app.repositories.services.StoryLikesService;
import ph.sprk.storycal.app.repositories.services.StoryService;
import ph.sprk.storycal.app.repositories.services.UserService;
import ph.sprk.storycal.app.utils.GenericResponse;

@RestController
@RequestMapping("/api")
public class StoryController {
	
	@Autowired
	private StoryService rep;
	
	@Autowired
	private StoryDAO story;
	
	@Autowired
	private NewsfeedDAO newsfeed;
	
	@Autowired
	private UserService service;
	
	@Autowired
	private StoryLikesService storyLikeService;
	
	@Autowired
	private StoryCommentsService commentService;
	
	
	@RequestMapping(value = "/create-story", method = RequestMethod.POST)
	public ResponseEntity<?> addPerson(@RequestBody UserStory story) {
		return new ResponseEntity<>(rep.saveStory(story), HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/get-all-story", method = RequestMethod.GET)
	public ResponseEntity<Collection<UserStory>> getStory() {
		return new ResponseEntity<>(rep.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get-story/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getStoryDetails(@PathVariable int id) throws Exception {
		return new ResponseEntity<>(story.getStoryDetails(id), HttpStatus.OK);
	}
	
	/**
	 * 
	 * Newsfeed API's
	 * 
	 * @param user_id    - user id of the current user
	 * @param page_no    - current page number
	 * @param page_size  - number of data to be display per page
	 * @return			 - return PagedResults
	 * @throws Exception
	 */
	@RequestMapping(value = "/get-newsfeed/{user_id}", method = RequestMethod.GET)
	public ResponseEntity<?> getNewsfeed(
			@PathVariable int user_id, @RequestParam("page") int page_no, 
			@RequestParam("page_size") int page_size, @RequestParam(value = "isPublic", required = false) boolean isPublic) throws Exception {
		return new ResponseEntity<>(newsfeed.generateNewsfeed(user_id, page_no, page_size, isPublic), HttpStatus.OK);
	}
	
	
	
	/**
	 * Likes section
	 */
	@RequestMapping(value = "/story/like", method = RequestMethod.POST)
	public ResponseEntity<?> likeStory(@RequestBody StoryLikes likes) throws Exception {
		int storyId = likes.getStoryId();
		int userId  = likes.getUserId();
		
		UserStory sl = rep.findByStoryId(storyId);
		User u = service.findByUserId(userId);
		
		if (userId == 0) {
			return new ResponseEntity<>(new GenericResponse(false, "Missing required parameter ::user_id"), HttpStatus.OK);
		}
		else if (storyId == 0) {
			return new ResponseEntity<>(new GenericResponse(false, "Missing required parameter ::story_id"), HttpStatus.OK);
		}
		else if (sl == null) {
			return new ResponseEntity<>(new GenericResponse(false, "Story cannot be found!"), HttpStatus.OK);
		}
		else if (u == null) {
			return new ResponseEntity<>(new GenericResponse(false, "User cannot be found"), HttpStatus.OK);
		}
		
		StoryLikes l = storyLikeService.save(likes);
		return new ResponseEntity<>(l, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/story/unlike/{story_id}/user/{user_id}", method = RequestMethod.POST)
	public ResponseEntity<?> unlikeStory(@PathVariable int story_id, @PathVariable int user_id) throws Exception {
		
		StoryLikes l = storyLikeService.findByStoryIdUserId(story_id, user_id);
		
		if (l == null) 
			return new ResponseEntity<>(new GenericResponse(false, "story cannot be found"), HttpStatus.BAD_REQUEST);
		
		int like_id = l.getLikeId();
		storyLikeService.deleteLike(like_id);
		
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
	/**
	 * 
	 * Comment Section
	 * 
	 */
	
	@RequestMapping(value = "/story/comment", method = RequestMethod.POST)
	public ResponseEntity<?> commentStory(@RequestBody StoryComments comments) throws Exception {
		int storyId = comments.getStoryId();
		int userId  = comments.getUserIdCommentator();
		
		UserStory sl = rep.findByStoryId(storyId);
		User u = service.findByUserId(userId);	
		

		if (userId == 0) {
			return new ResponseEntity<>(new GenericResponse(false, "Missing required parameter ::user_id"), HttpStatus.OK);
		}
		else if (storyId == 0) {
			return new ResponseEntity<>(new GenericResponse(false, "Missing required parameter ::story_id"), HttpStatus.OK);
		}
		else if (sl == null) {
			return new ResponseEntity<>(new GenericResponse(false, "Story cannot be found!"), HttpStatus.OK);
		}
		else if (u == null) {
			return new ResponseEntity<>(new GenericResponse(false, "User cannot be found"), HttpStatus.OK);
		}
		
		StoryComments c = commentService.save(comments);
		return new ResponseEntity<>(c, HttpStatus.OK);
	}
	
	
}
