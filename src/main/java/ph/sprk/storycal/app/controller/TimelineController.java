package ph.sprk.storycal.app.controller;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.sprk.storycal.app.repositories.dao.TimelineDAO;

@RestController
@RequestMapping("/api")
public class TimelineController {
	
	@Autowired
	private TimelineDAO timeline;
	
	@RequestMapping(path = "/get-timeline/{user_id}", method = RequestMethod.GET)
	public ResponseEntity<?> getTimeline(
			@PathVariable int user_id,
			@RequestParam(value = "groupBy", required = false) String groupBy,
			@RequestParam(value = "year", required = false) int year,
			@RequestParam(value = "month", required = false) int month) throws Exception {
		String [] groupValue = {"YEAR", "MONTH", "DAILY"};
		
		String filterGroupBy = (Arrays.asList(groupValue).indexOf(groupBy.toUpperCase()) == -1) ? "YEAR" : groupBy;
		int filterYear = filterGroupBy.equalsIgnoreCase("MONTH") ? ((year == 0) ? 2018 : year ) : year;
		int filterMonth = (month >= 1 || month <= 12) ? month : 1;
		
		return new ResponseEntity<Object>(timeline.getTimeline(user_id, filterGroupBy, filterYear, filterMonth), HttpStatus.OK);
	}
}
