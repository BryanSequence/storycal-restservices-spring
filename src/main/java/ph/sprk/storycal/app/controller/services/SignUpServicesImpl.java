package ph.sprk.storycal.app.controller.services;



import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ph.sprk.storycal.app.entities.User;
import ph.sprk.storycal.app.entities.UserAccount;
import ph.sprk.storycal.app.entities.Verification;
import ph.sprk.storycal.app.mapper.AccountResponse;
import ph.sprk.storycal.app.repositories.services.UserAccountService;
import ph.sprk.storycal.app.repositories.services.UserService;
import ph.sprk.storycal.app.repositories.services.VerificationCodeService;
import ph.sprk.storycal.app.utils.GenericResponse;
import ph.sprk.storycal.app.utils.PasswordUtils;
import ph.sprk.storycal.app.utils.SendGridEmailService;
import ph.sprk.storycal.app.utils.VerificationCodeGenerator;

/**
 * 
 * @author Bryan Judelle Ramos
 * 
 * @description
 * 	This services contains all business logic for the controller
 * 
 */


@Service
public class SignUpServicesImpl {
	
	@Autowired
	private UserService service;
	
	@Autowired
	private UserAccountService userAccountService;
	
	@Autowired
	private VerificationCodeService vcodeService;
	
	@Autowired 
	SendGridEmailService sendGridEmailService;
	
	@Autowired
	VerificationCodeGenerator vcode;
	
	
	public boolean isUserNameExist(String username) {
		UserAccount userAccount = userAccountService.findByUsername(username);
		return (userAccount == null) ? false : true;
	}
	
	public boolean isUserEmailExist(String email) {
		User user = service.findUserByEmail(email);
		return (user == null) ? false : true;
	}
	
	public Object verifyEmailAddress(String email) {
		
		String code = vcode.verificationCode(4);
		
		if (!this.isUserEmailExist(email)) {
			
			Verification vc = vcodeService.findByEmail(email);
			
			if (vc == null) {
				vc = new Verification();
				vc.setEmail(email);
				vc.setVerification_code(code);
				vcodeService.saveVCode(vc);
				
				try {
					this.sendGridEmailService.sendHTML(
							"bjmramos@gmail.com", email, 
							"StoryCal Verification Code", 
							"Hello, This is your 4 digit verification code <strong>" 
							+ code +"</strong>");
					
					return new GenericResponse(true, "verification code sent to your email");
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
				
			} else if (vc != null && vc.isVerified() == false) {
				
				vc.setVerification_code(code);
				vcodeService.saveVCode(vc);
				
				try {
					
					this.sendGridEmailService.sendHTML(
							"bjmramos@gmail.com", email, 
							"StoryCal Verification Code", 
							"Hello, This is your 4 digit verification code <strong>" 
							+ code +"</strong>");
					vcodeService.saveVCode(vc);
					
					return new GenericResponse(true, "verification code resend to your email");
	
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
			}
		} 
		
		return new GenericResponse(false, "This email address is already registered! please try again with another email"); 
	}
	
	public Object validateVerificationCode(String email, String code) {
		
		Verification v = vcodeService.findByEmail(email);
		
		if (v == null)
			return new GenericResponse(false, "Invalid Email Address!");
		else if (v != null && v.getVerification_code().equals(code) && v.isVerified()) 
			return new GenericResponse(false, "This verification code is already verified!");
		else if (v != null && !v.getVerification_code().equals(code) && v.isVerified() == false)
			return new GenericResponse(false, "Invalid verification code!");
		
		Date date = new Date();
		v.setVerified(true);
		v.setDateVerfied(date);
		vcodeService.saveVCode(v);

		return new GenericResponse(true, "Your verification code is correct! Welcome to StoryCal", v);
	}
	
	public Object createUserAccount(String email, String username, String password) throws Exception {
		
		Verification v = vcodeService.findByEmail(email);
		
		if (v == null) 
			return new GenericResponse(false, "Invalid Access");
		else if (v != null && !v.isVerified()) 
			return new GenericResponse(false, "Email is not yet verified! Please get your email address verified");
		
		PasswordUtils pw = new PasswordUtils();

		String hashed_password = pw.getSaltedHash(password);
		Date date = new Date();
		User u = service.saveUser(new User(email, new UserAccount(username, hashed_password, "ACTIVE", date)));
		
		AccountResponse response = new AccountResponse();
		
		response.setEmail(u.getEmail());
		response.setUsername(u.getUserAccount().getUsername());
		response.setUser_id(u.getUserId());
		response.setUser_account_id(u.getUserAccount().getUser_sec_id());
		
		return new GenericResponse(true, "Account Successfully Created!", response);
	}
	
	public Object updateUserProfile(int user_id, User u) {
		User userFromDB = service.findByUserId(user_id);
	
		copyNonNullProperties(u, userFromDB);
		User user = service.saveUser(userFromDB);
		return new GenericResponse(true, "Successfully Update User Profile", user);
	}
	
	
	/**
	 *  for Updating Specific Fields
	 *  TODO	merge two object the src and existing check for null values and ignore it
	 * 
	 * @param src    - the payload object
	 * @param target - existing object (came from the DB)
	 */
	public void copyNonNullProperties(Object src, Object target) {
	    BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
	}
	
	public static String[] getNullPropertyNames (Object source) {
	    final BeanWrapper src = new BeanWrapperImpl(source);
	    java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

	    Set<String> emptyNames = new HashSet<String>();
	    for (java.beans.PropertyDescriptor pd : pds) {
	        Object srcValue = src.getPropertyValue(pd.getName());
	        if (srcValue instanceof Boolean) {
	        	if ((boolean) srcValue == false) 
	        		emptyNames.add(pd.getName());
	        } else if(srcValue instanceof Integer) {
	        	if ((Integer) srcValue == 0) 
	        		emptyNames.add(pd.getName());
	        } else {
	        	if (srcValue == null) 
	        		emptyNames.add(pd.getName());
	        }
	    }
	    String[] result = new String[emptyNames.size()];
	    return emptyNames.toArray(result);
	}
	
}
