package ph.sprk.storycal.app.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="follow", schema = "storycal_db")
public class Friendship {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="follow_id")
	private int followId;
	
	@Column(name="user_id")
	private int userId;
	
	@Column(name="following_id")
	private int followingId;
	
	@Column(name="dateFollowing")
	private Date dateFollowing;

	public int getFollowId() {
		return followId;
	}

	public void setFollowId(int followId) {
		this.followId = followId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getFollowingId() {
		return followingId;
	}

	public void setFollowingId(int followingId) {
		this.followingId = followingId;
	}

	public Date getDateFollowing() {
		return dateFollowing;
	}

	public void setDateFollowing(Date dateFollowing) {
		this.dateFollowing = dateFollowing;
	}
	
}
