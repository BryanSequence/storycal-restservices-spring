package ph.sprk.storycal.app.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "story_category_reference")
public class StoryCategoryRef {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="category_id")
	private int categoryId;
	
	@Column(name="story_category")
	private String storyCategory;
	
	@Column(name="date_created")
	private Date dateCreated;
	
	@ManyToMany(cascade = CascadeType.MERGE)
	@JsonBackReference
	@JoinTable(name = "story_category_lookup",
		joinColumns = @JoinColumn(name = "category_id", referencedColumnName = "category_id"),
		inverseJoinColumns = @JoinColumn(name = "story_id", referencedColumnName = "story_id"))
	private Set<UserStory> userStory = new HashSet<UserStory>();
	

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getStoryCategory() {
		return storyCategory;
	}

	public void setStoryCategory(String storyCategory) {
		this.storyCategory = storyCategory;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Set<UserStory> getUserStory() {
		return userStory;
	}

	public void setUserStory(Set<UserStory> userStory) {
		this.userStory = userStory;
	}


	
}
