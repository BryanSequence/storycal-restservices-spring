package ph.sprk.storycal.app.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="story_comments", schema = "storycal_db")
public class StoryComments {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="comment_id")
	private int commentId;
	
	@Column(name="story_id")
	private int storyId;
	
	@Column(name="user_id_commentator")
	private int userIdCommentator;
	
	private String comment;
	
	@Column(name="date_comment")
	private Date dateComment;

	public int getCommentId() {
		return commentId;
	}

	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	public int getUserIdCommentator() {
		return userIdCommentator;
	}

	public void setUserIdCommentator(int userIdCommentator) {
		this.userIdCommentator = userIdCommentator;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getDateComment() {
		return dateComment;
	}

	public void setDateComment(Date dateComment) {
		this.dateComment = dateComment;
	}

	public int getStoryId() {
		return storyId;
	}

	public void setStoryId(int storyId) {
		this.storyId = storyId;
	}
}
