package ph.sprk.storycal.app.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="story_likes", schema = "storycal_db")
public class StoryLikes {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="like_id")
	private int likeId;
	
	@Column(name="story_id")
	private int storyId;
	
	@Column(name="user_id")
	private int userId;
	
	@Column(name="date_liked")
	private Date dateLiked;

	public int getLikeId() {
		return likeId;
	}

	public void setLikeId(int likeId) {
		this.likeId = likeId;
	}

	public int getStoryId() {
		return storyId;
	}

	public void setStoryId(int storyId) {
		this.storyId = storyId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Date getDateLiked() {
		return dateLiked;
	}

	public void setDateLiked(Date dateLiked) {
		this.dateLiked = dateLiked;
	}
}
