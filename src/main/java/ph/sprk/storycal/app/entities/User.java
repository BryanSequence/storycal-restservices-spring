package ph.sprk.storycal.app.entities;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_id")
	private int userId;
	
	private String email;
	
	private String name;
	
	private String gender;
	
	private Date birthday;
	
	private String about;
	
	private String interest;
	
	private String address;
	
	private String picture_url;
	
	private String cover_photo_url;
	
	private String thumb_url;
	
	private boolean is_verified;
	
	private boolean is_account_set;
	
	private boolean is_profile_set;
	
	private String fuid;
	
	@OneToOne(targetEntity=UserAccount.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "user_sec_id")
	@JsonIgnore
	private UserAccount userAccount;
	
	public User() {}
	
	
	public User(String email) {
		this.email = email;
	}
	
	public User(String email, UserAccount userAccount) {
		this.email = email;
		this.userAccount = userAccount;
	}
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
	public String getAbout() {
		return about;
	}
	public void setAbout(String about) {
		this.about = about;
	}
	
	public String getInterest() {
		return interest;
	}
	public void setInterest(String website) {
		this.interest = website;
	}
	
	public String getPicture_url() {
		return picture_url;
	}
	public void setPicture_url(String picture_url) {
		this.picture_url = picture_url;
	}
	
	public boolean isVerified() {
		return is_verified;
	}
	public void setVerified(boolean is_verified) {
		this.is_verified = is_verified;
	}
	
	public boolean isAccountSet() {
		return is_account_set;
	}
	public void setAccountSet(boolean is_account_set) {
		this.is_account_set = is_account_set;
	}
	
	public boolean isProfileSet() {
		return is_profile_set;
	}
	public void setProfileSet(boolean is_profile_set) {
		this.is_profile_set = is_profile_set;
	}
	public String getFuid() {
		return fuid;
	}
	public void setFuid(String fuid) {
		this.fuid = fuid;
	}
	
	public UserAccount getUserAccount() {
		return userAccount;
	}
	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}


	public String getThumb_url() {
		return thumb_url;
	}


	public void setThumb_url(String thumb_url) {
		this.thumb_url = thumb_url;
	}


	public String getCover_photo_url() {
		return cover_photo_url;
	}


	public void setCover_photo_url(String cover_photo_url) {
		this.cover_photo_url = cover_photo_url;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}
	
	
}

