package ph.sprk.storycal.app.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="user_account", schema = "storycal_db")
public class UserAccount {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int user_sec_id;
	
	private String username;
	
	private String password;
	
	private String account_status;
	
	private Date date_created;
	
	private Date date_updated;
	
	@OneToOne(mappedBy = "userAccount")
	@JsonIgnore
	private User user;
	
	public UserAccount () {}
	
	public UserAccount(String username) {
		this.username = username;
	}
	
	public UserAccount(String username, String password, String account_status, Date date_created) {
		this.username = username;
		this.password = password;
		this.account_status = account_status;
		this.date_created = date_created;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getAccount_status() {
		return account_status;
	}

	public void setAccount_status(String account_status) {
		this.account_status = account_status;
	}
	
	public Date getDate_created() {
		return date_created;
	}

	public void setDate_created(Date date_created) {
		this.date_created = date_created;
	}

	public Date getDate_updated() {
		return date_updated;
	}

	public void setDate_updated(Date date_updated) {
		this.date_updated = date_updated;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getUser_sec_id() {
		return user_sec_id;
	}

	public void setUser_sec_id(int user_sec_id) {
		this.user_sec_id = user_sec_id;
	}

}
