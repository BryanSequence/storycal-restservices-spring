package ph.sprk.storycal.app.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "user_story")
public class UserStory {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="story_id")
	private int storyId;
	
	@Column(name="user_id")
	private int userId;
	
	private String title;
	
	private String story;
	
	@Column(name="photo_url")
	private String photoUrl;
	
	private String privacy;
	
	@Column(name="date_posted")
	private Date datePosted;

	@ManyToMany(cascade = CascadeType.MERGE)
	@JsonBackReference
	@JoinTable(name = "story_category_lookup",
		joinColumns = @JoinColumn(name = "story_id", referencedColumnName = "story_id"),
		inverseJoinColumns = @JoinColumn(name = "category_id", referencedColumnName = "category_id"))
	private Set<StoryCategoryRef> category = new HashSet<StoryCategoryRef>();
	
	public int getStoryId() {
		return storyId;
	}

	public void setStoryId(int storyId) {
		this.storyId = storyId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getStory() {
		return story;
	}

	public void setStory(String story) {
		this.story = story;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public Date getDatePosted() {
		return datePosted;
	}

	public void setDatePosted(Date datePosted) {
		this.datePosted = datePosted;
	}

	public String getPrivacy() {
		return privacy;
	}

	public void setPrivacy(String privacy) {
		this.privacy = privacy;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Set<StoryCategoryRef> getCategory() {
		return category;
	}

	public void setCategory(Set<StoryCategoryRef> category) {
		this.category = category;
	}
}
