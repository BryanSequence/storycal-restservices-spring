package ph.sprk.storycal.app.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Verification {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int vcode_id;
	
	private String verification_code;
	
	private String email;
	
	private boolean verified;
	
	private Date date_verified;

	public int getVcode_id() {
		return vcode_id;
	}

	public void setVcode_id(int vcode_id) {
		this.vcode_id = vcode_id;
	}

	public String getVerification_code() {
		return verification_code;
	}

	public void setVerification_code(String verification_code) {
		this.verification_code = verification_code;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public Date getDateVerfied() {
		return date_verified;
	}

	public void setDateVerfied(Date dateVerfied) {
		this.date_verified = dateVerfied;
	}
	
	
}
