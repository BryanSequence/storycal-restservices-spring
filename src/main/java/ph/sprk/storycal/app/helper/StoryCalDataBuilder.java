package ph.sprk.storycal.app.helper;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

/**
 *  Design Pattern: Template Design Pattern
 * 
 * 
 * @author Bryan Judelle Ramos
 * @param <T>
 *
 */

public abstract class StoryCalDataBuilder<T> {
	
	private List<Object[]> objList;
	
	public final List<T> generate() {
		return this.constructData(this.getObjList());
	}
	
	@SuppressWarnings("unchecked")
	public void fetchDataFromStoredProcedureById(EntityManager em,String storedProcName, int... argsIds) {
		
		StoredProcedureQuery query = em.createStoredProcedureQuery(storedProcName);
		
		for (int index = 0; index < argsIds.length; index++) {
			int param = index + 1;
			query.registerStoredProcedureParameter(param, Integer.class, ParameterMode.IN);
			query.setParameter(param, argsIds[index]);
		}
		
		query.execute();
		
		this.setObjList(query.getResultList());
	}
	
	//adaptable abstract method
	public abstract List<T> constructData(List<Object[]> obj);
	

	/**
	 * Setter and Getter
	 */
	public List<Object[]> getObjList() {
		return objList;
	}

	public void setObjList(List<Object[]> objList) {
		this.objList = objList;
	}

	
	
}
