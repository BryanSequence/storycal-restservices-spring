package ph.sprk.storycal.app.mapper;

import ph.sprk.storycal.app.entities.User;
import ph.sprk.storycal.app.entities.UserAccount;

public class AccountRequest {
	private User user;
	private UserAccount userAccount;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public UserAccount getUserAccount() {
		return userAccount;
	}
	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}
}
