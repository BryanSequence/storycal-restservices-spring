package ph.sprk.storycal.app.mapper;


public class AccountResponse {
	private int user_id;
	
	private int user_account_id;
	
	private String email;
	
	private String username;
	
	public int getUser_id() {
		return user_id;
	}
	
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	
	public int getUser_account_id() {
		return user_account_id;
	}
	
	public void setUser_account_id(int user_account_id) {
		this.user_account_id = user_account_id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	
	
}
