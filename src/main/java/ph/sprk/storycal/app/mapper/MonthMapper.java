package ph.sprk.storycal.app.mapper;

public class MonthMapper {
	
	private int year;
	
	private Object monthList;

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Object getMonthList() {
		return monthList;
	}

	public void setMonthList(Object monthList) {
		this.monthList = monthList;
	}
	
}
