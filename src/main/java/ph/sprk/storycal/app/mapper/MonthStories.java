package ph.sprk.storycal.app.mapper;

public class MonthStories {

	private int monthNumber;
	
	private String monthName;
	
	private String latestStoryPicture;
	
	private String latestStory;

	public int getMonthNumber() {
		return monthNumber;
	}

	public void setMonthNumber(int monthNumber) {
		this.monthNumber = monthNumber;
	}

	public String getMonthName() {
		return monthName;
	}

	public void setMonthName(String monthName) {
		this.monthName = monthName;
	}

	public String getLatestStoryPicture() {
		return latestStoryPicture;
	}

	public void setLatestStoryPicture(String latestStoryPicture) {
		this.latestStoryPicture = latestStoryPicture;
	}

	public String getLatestStory() {
		return latestStory;
	}

	public void setLatestStory(String latestStory) {
		this.latestStory = latestStory;
	}
}
