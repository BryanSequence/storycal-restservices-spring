package ph.sprk.storycal.app.mapper;

import java.util.ArrayList;
import java.util.List;

public class PagedResult {
	
	public static final long DEFAULT_OFFSET = 0;
	public static final int DEFAULT_MAX_NO_OF_ROWS = 100;
    
	private int offset;
    private int limit;
    private long totalPages;
    private boolean isPublic;
   
    private List<Newsfeed> newsfeed;
    
    public PagedResult(ArrayList<Newsfeed> newsfeed, long totalPage, int offset, int limit, boolean isPublic) {
    	this.setNewsfeed(newsfeed);
        this.totalPages = totalPage;
        this.offset = offset;
        this.limit = limit;
        this.setPublic(isPublic);
    }
    
    public boolean hasMore() {
        return totalPages > offset + limit;
    }
    
    public boolean hasPrevious() {
        return offset > 0 && totalPages > 0;
    }
    
    public long getTotalPages() {
        return totalPages;
    }
    
    public int  getOffset() {
        return offset;
    }
    
    public int getLimit() {
        return limit;
    }

	public boolean isPublic() {
		return isPublic;
	}

	public void setPublic(boolean isPublic) {
		this.isPublic = isPublic;
	}

	public List<Newsfeed> getNewsfeed() {
		return newsfeed;
	}

	public void setNewsfeed(List<Newsfeed> newsfeed) {
		this.newsfeed = newsfeed;
	}
}
