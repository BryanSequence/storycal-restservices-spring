package ph.sprk.storycal.app.mapper;

public class StoryCategory {
	private int categoryId;
	
	private String storyCategory;
	
	public int getCategoryId() {
		return categoryId;
	}
	
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	
	public String getStoryCategory() {
		return storyCategory;
	}
	public void setStoryCategory(String storyCategory) {
		this.storyCategory = storyCategory;
	}
	
}
