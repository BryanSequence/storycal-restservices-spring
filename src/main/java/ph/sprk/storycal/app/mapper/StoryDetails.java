package ph.sprk.storycal.app.mapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class StoryDetails {

	private int storyId;
	
	private int userId;
	
	private String title;
	
	private String story;
	
	private String photo_url;
	
	private String privacy;
	
	private Date datePosted;
	
	private ArrayList<HashMap<String, Object>> storyCategories;
	
	private ArrayList<HashMap<String, Object>> storyLikes;

	private ArrayList<HashMap<String, Object>> storyComments;

	public int getStoryId() {
		return storyId;
	}

	public void setStoryId(int storyId) {
		this.storyId = storyId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStory() {
		return story;
	}

	public void setStory(String story) {
		this.story = story;
	}

	public String getPhoto_url() {
		return photo_url;
	}

	public void setPhoto_url(String photo_url) {
		this.photo_url = photo_url;
	}

	public String getPrivacy() {
		return privacy;
	}

	public void setPrivacy(String privacy) {
		this.privacy = privacy;
	}

	public Date getDatePosted() {
		return datePosted;
	}

	public void setDatePosted(Date datePosted) {
		this.datePosted = datePosted;
	}

	public ArrayList<HashMap<String, Object>> getStoryCategories() {
		return storyCategories;
	}

	public void setStoryCategories(ArrayList<HashMap<String, Object>> storyCategories) {
		this.storyCategories = storyCategories;
	}

	public ArrayList<HashMap<String, Object>> getStoryLikes() {
		return storyLikes;
	}

	public void setStoryLikes(ArrayList<HashMap<String, Object>> storyLikes) {
		this.storyLikes = storyLikes;
	}

	public ArrayList<HashMap<String, Object>> getStoryComments() {
		return storyComments;
	}

	public void setStoryComments(ArrayList<HashMap<String, Object>> storyComments) {
		this.storyComments = storyComments;
	}
	
}
