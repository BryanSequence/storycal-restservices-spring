package ph.sprk.storycal.app.mapper;

import java.util.List;

public class Timeline {
	
	private List<YearStories> yearStories;

	public List<YearStories> getYearStories() {
		return yearStories;
	}

	public void setYearStories(List<YearStories> yearStories) {
		this.yearStories = yearStories;
	}
	
}
