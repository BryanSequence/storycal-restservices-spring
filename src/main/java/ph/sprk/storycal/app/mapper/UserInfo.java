package ph.sprk.storycal.app.mapper;

/**
 * 
 * @author Bryan Judelle Ramos
 * 
 * 		class mapper for User information for newsfeed items
 *
 */

public class UserInfo {
	
	private int userId;
	
	private String name;
	
	private String photo_url;
		
	public UserInfo() {}
	
	public UserInfo(int userId, String name, String photo_url) {
		this.setUserId(userId);
		this.setName(name);
		this.setPhoto_url(photo_url);
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoto_url() {
		return photo_url;
	}

	public void setPhoto_url(String photo_url) {
		this.photo_url = photo_url;
	}

//	public String getCover_url() {
//		return cover_url;
//	}
//
//	public void setCover_url(String cover_url) {
//		this.cover_url = cover_url;
//	}

}
