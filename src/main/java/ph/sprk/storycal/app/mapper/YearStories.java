package ph.sprk.storycal.app.mapper;

public class YearStories {
	
	private int year;
	
	private String latestStoryPicture;
	
	private String latestStory;

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getLatestStoryPicture() {
		return latestStoryPicture;
	}

	public void setLatestStoryPicture(String latestStoryPicture) {
		this.latestStoryPicture = latestStoryPicture;
	}

	public String getLatestStory() {
		return latestStory;
	}

	public void setLatestStory(String latestStory) {
		this.latestStory = latestStory;
	}
	
	
}
