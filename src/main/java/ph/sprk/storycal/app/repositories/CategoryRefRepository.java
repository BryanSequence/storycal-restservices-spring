package ph.sprk.storycal.app.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import ph.sprk.storycal.app.entities.StoryCategoryRef;

public interface CategoryRefRepository extends CrudRepository<StoryCategoryRef, Long>{

	@SuppressWarnings("unchecked")
	public StoryCategoryRef save(StoryCategoryRef storyCatRef);
	
	public StoryCategoryRef findByCategoryId(int category_id);
	
	public StoryCategoryRef findByStoryCategory(String category);
	
	public List<StoryCategoryRef> findAll();

}
