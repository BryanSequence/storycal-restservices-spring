package ph.sprk.storycal.app.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ph.sprk.storycal.app.entities.Friendship;

public interface FriendshipRepository extends CrudRepository<Friendship, Integer> {
	@SuppressWarnings("unchecked")
	public Friendship save(Friendship friend);
	
	public Friendship findByUserId(int userId);
	
	public Friendship findByFollowingId(int followingId);
	
	@Query("SELECT s FROM Friendship s WHERE s.userId = ?1 AND s.followingId = ?2")
	public Friendship findByUserIdFollowing(int user_id,int following_id);
}
