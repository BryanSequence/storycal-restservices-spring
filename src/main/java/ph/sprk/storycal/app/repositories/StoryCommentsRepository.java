package ph.sprk.storycal.app.repositories;

import org.springframework.data.repository.CrudRepository;

import ph.sprk.storycal.app.entities.StoryComments;

public interface StoryCommentsRepository extends CrudRepository<StoryComments, Long> {
	@SuppressWarnings("unchecked")
	public StoryComments save(StoryComments storyComments);
	
	public StoryComments findByCommentId(int commentId);
	
}
