package ph.sprk.storycal.app.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ph.sprk.storycal.app.entities.StoryLikes;

public interface StoryLikesRepository extends CrudRepository<StoryLikes, Integer> {
	@SuppressWarnings("unchecked")
	public StoryLikes save(StoryLikes story);
	
	public StoryLikes findByLikeId(int like_id);
	
	@Query("SELECT s FROM StoryLikes s WHERE s.storyId = ?1 AND s.userId = ?2")
	public StoryLikes findByStoryIdUserId(int story_id,int user_id);
	
}
