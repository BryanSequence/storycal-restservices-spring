package ph.sprk.storycal.app.repositories;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;


import ph.sprk.storycal.app.entities.UserStory;

public interface StoryRepository extends CrudRepository<UserStory, Long> {
	
	@SuppressWarnings("unchecked")
	public UserStory save(UserStory userStory);
	
	public UserStory findByStoryId(int story_id);
	
	public UserStory findByUserId(int user_id);
	
	public Collection<UserStory> findAll();
	
	
}
