package ph.sprk.storycal.app.repositories;

import org.springframework.data.repository.CrudRepository;

import ph.sprk.storycal.app.entities.UserAccount;

public interface UserAccountRepository extends CrudRepository<UserAccount, Long>{
	
	@SuppressWarnings("unchecked")
	public UserAccount save(UserAccount userAccount);
	
	public UserAccount findByUsername(String username);
}
