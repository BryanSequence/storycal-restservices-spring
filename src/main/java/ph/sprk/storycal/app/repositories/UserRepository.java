package ph.sprk.storycal.app.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ph.sprk.storycal.app.entities.User;

public interface UserRepository extends CrudRepository<User, Long> {
	
	@SuppressWarnings("unchecked")
	public User save(User user);
	
	public User findByEmail(String email);
	
	public User findByUserId(int user_id);
	
	@Query("SELECT u FROM User u WHERE u.name LIKE %:name%")
	public List<User> findByName(@Param("name") String name);
	
	@Query("SELECT u FROM User u WHERE user_sec_id = :sec_id")
	public User findByUserSecId(@Param("sec_id") int sec_id);
	
}
