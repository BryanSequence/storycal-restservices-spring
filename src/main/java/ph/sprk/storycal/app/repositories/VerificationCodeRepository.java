package ph.sprk.storycal.app.repositories;

import org.springframework.data.repository.CrudRepository;
import ph.sprk.storycal.app.entities.Verification;

public interface VerificationCodeRepository extends CrudRepository<Verification, Long>{
	@SuppressWarnings("unchecked")
	public Verification save(Verification vcode);
	public Verification findByEmail(String email);
}
