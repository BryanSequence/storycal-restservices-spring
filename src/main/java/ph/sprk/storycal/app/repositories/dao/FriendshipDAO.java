package ph.sprk.storycal.app.repositories.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class FriendshipDAO {
	@Autowired
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public Object getFollowing(int user_id) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		
		String sql = "SELECT f.following_id, u.name FROM follow f JOIN user u ON f.following_id = u.user_id WHERE f.user_id = :user_id";
		Query u = entityManager.createNativeQuery(sql);
		u.setParameter("user_id", user_id);
		
		ArrayList<HashMap<String, Object>> followingList = new ArrayList<HashMap<String, Object>>();
		List<Object[]> listFollowing = u.getResultList();
		
		if (listFollowing.size() <= 0) {
			listFollowing = null;
		} else {
			@SuppressWarnings("rawtypes")
			Iterator it = listFollowing.iterator();
			
			while (it.hasNext()) {
				Object[] line = (Object[]) it.next();
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("userId", line[0].toString());
				map.put("name", line[1].toString());
				followingList.add(map);
			}			
		}
		
		return followingList;
	}
	
	@SuppressWarnings("unchecked")
	public Object getFollowers(int following_id) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		
		String sql = "SELECT f.user_id, u.name FROM follow f JOIN user u ON f.user_id = u.user_id WHERE f.following_id = :following_id";
		Query u = entityManager.createNativeQuery(sql);
		u.setParameter("following_id", following_id);
		
		ArrayList<HashMap<String, Object>> followersList = new ArrayList<HashMap<String, Object>>();
		List<Object[]> listFollowers = u.getResultList();
		
		if (listFollowers.size() <= 0) {
			listFollowers = null;
		} else {
			@SuppressWarnings("rawtypes")
			Iterator it = listFollowers.iterator();
			
			while (it.hasNext()) {
				Object[] line = (Object[]) it.next();
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("userId", line[0].toString());
				map.put("name", line[1].toString());
				followersList.add(map);
			}			
		}
		
		return followersList;
	}
}
