package ph.sprk.storycal.app.repositories.dao;


import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ph.sprk.storycal.app.mapper.PagedResult;
import ph.sprk.storycal.app.utils.NewsfeedUtils;

@Transactional
@Repository
public class NewsfeedDAO {

	@Autowired
	private EntityManager entityManager;
	
	public PagedResult generateNewsfeed(int userId, int pageNumber, int pageSize, boolean isPublic) {
		return new NewsfeedUtils().buildNewsFeed(entityManager, userId, pageNumber, pageSize, isPublic);
	}
}












