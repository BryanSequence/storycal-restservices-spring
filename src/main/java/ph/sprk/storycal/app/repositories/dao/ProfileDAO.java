package ph.sprk.storycal.app.repositories.dao;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class ProfileDAO {
	
	@Autowired
	private EntityManager entityManager;
	
	public Object updatePassword(int userSecId,String newPassword) {
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("update_password");
		query.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN);
		query.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
		query.setParameter(1, userSecId);
		query.setParameter(2, newPassword);
		
		return query.execute();
	}

}
