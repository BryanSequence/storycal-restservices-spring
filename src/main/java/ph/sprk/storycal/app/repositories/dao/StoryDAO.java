package ph.sprk.storycal.app.repositories.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ph.sprk.storycal.app.entities.UserStory;
import ph.sprk.storycal.app.mapper.StoryDetails;

@Transactional
@Repository
public class StoryDAO {
	
	@Autowired
	private EntityManager entityManager;
	
	public Object getStoryDetails(int storyId) {
		
		StoryDetails story = new StoryDetails();
		
		entityManager.getEntityManagerFactory().getCache().evictAll();

		String sql = "SELECT scr.category_id, scr.story_category FROM story_category_lookup scl " + 
					 "LEFT JOIN story_category_reference scr ON scl.category_id = scr.category_id " + 
					 "JOIN user_story us ON us.story_id = scl.story_id WHERE us.story_id = :story_id";
		
		String likeSQL = "SELECT sl.user_id, u.name FROM story_likes sl JOIN storycal_db.user u ON u.user_id = sl.user_id where sl.story_id = :story_id"; 
	
		String commentSQL = "SELECT sc.user_id_commentator, u.name, ua.username, IFNULL(u.picture_url,'N/A') as pic_url, IFNULL(u.cover_photo_url,'N/A') as cover_url, sc.comment, sc.date_comment FROM story_comments sc JOIN user u ON u.user_id = sc.user_id_commentator JOIN user_account ua ON ua.user_sec_id = u.user_sec_id where sc.story_id = :story_id";
		
		String [] catArr 	 = {"categoryId", "storyCategory"};
		String [] likeArr 	 = {"userId", "name"};
		String [] commentArr = {"userId", "name", "username", "picture_url", "cover_photo_url", "comments", "date"};
		
		UserStory s = entityManager.find(UserStory.class, storyId);
		
		story.setStoryId(s.getStoryId());
		story.setUserId(s.getUserId());
		story.setTitle(s.getTitle());
		story.setStory(s.getStory());
		story.setPrivacy(s.getPrivacy());
		story.setPhoto_url(s.getPhotoUrl());
		story.setDatePosted(s.getDatePosted());	
		story.setStoryCategories(storyDataBuilder(sql, storyId, catArr));
		story.setStoryLikes(storyDataBuilder(likeSQL, storyId, likeArr));
		story.setStoryComments(storyDataBuilder(commentSQL, storyId, commentArr));
		
		return story;
	}
	
	/**
	 *  storyDataBuilder - private
	 *  
	 *  	This sub-routine are used to build the story response object, which contains story details, likes details
	 *  	and comments details.
	 * 
	 * @param sql      - The SQL query use to fetch data for specific modules.
	 * @param storyId  - The unique identification of user story
	 * @param fieldMap - The fields for the hashmap, usually is refers to the specific field that selected on the SQL queries
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private ArrayList<HashMap<String, Object>> storyDataBuilder(String sql, int storyId, String fieldMap[]) {
		ArrayList<HashMap<String, Object>> listObject = new ArrayList<HashMap<String, Object>>();
		
		Query entity = entityManager.createNativeQuery(sql);
			  entity.setParameter("story_id", storyId);
		
			  List<Object[]> obj = entity.getResultList();
		
		if (obj == null) 
			listObject = null;
		else {
			
			Iterator objList = obj.iterator();
			
			while (objList.hasNext()) {
				
				Object[] line  = (Object[]) objList.next();
				HashMap<String, Object> mapObj = new HashMap<String, Object>();
				
				for (int i = 0; i < fieldMap.length; i++) 
					mapObj.put(fieldMap[i], line[i].toString());
				
				listObject.add(mapObj);
			}
		}
		
		return listObject;
	}
	
}
