package ph.sprk.storycal.app.repositories.dao;

import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ph.sprk.storycal.app.helper.StoryCalDataBuilder;
import ph.sprk.storycal.app.mapper.MonthMapper;
import ph.sprk.storycal.app.mapper.YearMapper;
import ph.sprk.storycal.app.repositories.dao.adapter.DailyStoriesImpl;
import ph.sprk.storycal.app.repositories.dao.adapter.MonthStoriesImpl;
import ph.sprk.storycal.app.repositories.dao.adapter.TimelineImpl;

@Transactional
@Repository
public class TimelineDAO {
	
	@Autowired
	private EntityManager entityManager;
	
	private StoryCalDataBuilder<?> timeline;
	
	private Object responseObject;
	
	private MonthMapper mp;
	
	private YearMapper yp;
	
	public Object getTimeline(int userId, String groupBy, int year, int month) {
				
		switch (groupBy.toUpperCase()) {
			
			case "YEAR":
				timeline = new TimelineImpl();
				timeline.fetchDataFromStoredProcedureById(entityManager, "timeline_groupby_year", userId);
				yp = new YearMapper();
				yp.setYearList(timeline.generate());
				this.setResponseObject(yp);
				break;
			
			case "MONTH":
				timeline = new MonthStoriesImpl();
				timeline.fetchDataFromStoredProcedureById(entityManager, "timeline_groupby_month", userId, year);
				mp = new MonthMapper();
				mp.setYear(year);
				mp.setMonthList(timeline.generate());
				this.setResponseObject(mp);
				break;
				
			case "DAILY":
				timeline = new DailyStoriesImpl();
				timeline.fetchDataFromStoredProcedureById(entityManager, "timeline_groupby_daily", userId, year, month);
				this.setResponseObject(timeline.generate());
		}
		
		return this.getResponseObject();
	}

	public Object getResponseObject() {
		return responseObject;
	}

	public void setResponseObject(Object responseObject) {
		this.responseObject = responseObject;
	}

}
