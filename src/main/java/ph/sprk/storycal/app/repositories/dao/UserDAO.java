package ph.sprk.storycal.app.repositories.dao;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ph.sprk.storycal.app.mapper.ProfileSummary;

@Transactional
@Repository
public class UserDAO {
	
	@Autowired
	private EntityManager entityManager;
		
	@SuppressWarnings("unchecked")
	public Object getProfileSummary(int userId) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		ProfileSummary profileSummary = new ProfileSummary();
		
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("profile_summary");
		query.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN);
		query.setParameter(1, userId);
		query.execute();
		
		List<Object[]> listObj = query.getResultList();
		
		listObj.forEach(o -> {
			 profileSummary.setUserId((int) o[0]); 
			  profileSummary.setName(o[1].toString());
			  profileSummary.setUsername(o[2].toString());
			  profileSummary.setEmail(o[3].toString());
			  profileSummary.setAddress(o[4].toString());
			  profileSummary.setBday(o[5].toString());
			  profileSummary.setAbout(o[6].toString());
			  profileSummary.setPhotoURL(o[7].toString());
			  profileSummary.setCoverURL(o[8].toString());
			  profileSummary.setThumbURL(o[9].toString());
			  profileSummary.setFollowingCount(new BigInteger(o[10].toString()).intValue());
			  profileSummary.setFollowersCount(new BigInteger(o[11].toString()).intValue());
			  profileSummary.setStoriesCount(new BigInteger(o[12].toString()).intValue());
			  profileSummary.setFollow(false);
		});
		return profileSummary;
	}
	
	@SuppressWarnings("unchecked")
	public Object viewProfile(int userId, int profileId) {
		entityManager.getEntityManagerFactory().getCache().evictAll();
		ProfileSummary profileSummary = new ProfileSummary();
		
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("view_profile");
		query.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN);
		query.registerStoredProcedureParameter(2, Integer.class, ParameterMode.IN);
		query.setParameter(1, userId);
		query.setParameter(2, profileId);
		query.execute();
		
		List<Object[]> listObj = query.getResultList();
		
		listObj.forEach(o -> {
			 profileSummary.setUserId((int) o[0]); 
			  profileSummary.setName(o[1].toString());
			  profileSummary.setUsername(o[2].toString());
			  profileSummary.setEmail(o[3].toString());
			  profileSummary.setAddress(o[4].toString());
			  profileSummary.setBday(o[5].toString());
			  profileSummary.setAbout(o[6].toString());
			  profileSummary.setPhotoURL(o[7].toString());
			  profileSummary.setCoverURL(o[8].toString());
			  profileSummary.setThumbURL(o[9].toString());
			  profileSummary.setFollowingCount(new BigInteger(o[10].toString()).intValue());
			  profileSummary.setFollowersCount(new BigInteger(o[11].toString()).intValue());
			  profileSummary.setStoriesCount(new BigInteger(o[12].toString()).intValue());
			  profileSummary.setFollow(o[13].toString().equalsIgnoreCase("true") ? true : false);
		});
		return profileSummary;
	}
}
