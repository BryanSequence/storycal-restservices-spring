package ph.sprk.storycal.app.repositories.dao.adapter;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ph.sprk.storycal.app.helper.StoryCalDataBuilder;
import ph.sprk.storycal.app.mapper.DailyStories;
import ph.sprk.storycal.app.mapper.Story;
import ph.sprk.storycal.app.mapper.UserInfo;

public class DailyStoriesImpl extends StoryCalDataBuilder<DailyStories> {

	private int year;
	
	private int month;
	
	@Override
	public List<DailyStories> constructData(List<Object[]> obj) {
		
		String [] monthName = {
				"January", "February", "March", "April",
				"May", "June", "July", "August",
				"September", "October", "November", "December"
		};
		
		List<Story> storyList = new ArrayList<Story>();
		List<DailyStories> dailyStories = new ArrayList<DailyStories>();
		
		obj.forEach(item -> {
			UserInfo userInfo = new UserInfo();
			Story story = new Story();
			
			this.setYear((int) item[0]);
			this.setMonth((int) item[1]);
			
			userInfo.setUserId(this.bigIntToInteger(item[3]));
			userInfo.setName(item[4].toString());
			userInfo.setPhoto_url(item[5].toString());
			
			story.setYear((int) item[0]);
			story.setMonth((int) item[1]);
			story.setDay((int) item[2]);
			story.setUserInfo(userInfo);
			story.setStoryId(this.bigIntToInteger(item[6]));
			story.setTitle(item[7].toString());
			story.setStory(item[8].toString());
			story.setPhoto_url(item[9].toString());
			story.setPrivacy(item[10].toString());
			story.setDatePosted(item[11].toString());
			story.setLike_count(this.bigIntToInteger(item[12]));
			story.setComment_count(this.bigIntToInteger(item[13]));
			story.setIs_liked(item[14].toString().equals("true") ? true : false);
			
			storyList.add(story);
		});
		
		DailyStories daily = new DailyStories();
		
		daily.setYear(this.getYear());
		daily.setMonth(this.getMonth());
		daily.setMonthName(Arrays.asList(monthName).get(this.getMonth()-1));
		daily.setStoryList(storyList);
		
		dailyStories.add(daily);	
		
		return dailyStories;
	}

	private int  bigIntToInteger(Object number) {
		return new BigInteger(number.toString()).intValue();
	}
	
	
	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

}
