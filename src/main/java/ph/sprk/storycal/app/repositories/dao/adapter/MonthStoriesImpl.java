package ph.sprk.storycal.app.repositories.dao.adapter;

import java.util.ArrayList;
import java.util.List;

import ph.sprk.storycal.app.helper.StoryCalDataBuilder;
import ph.sprk.storycal.app.mapper.MonthStories;

public class MonthStoriesImpl extends StoryCalDataBuilder<MonthStories> {

	@Override
	public List<MonthStories> constructData(List<Object[]> obj) {
		List<MonthStories> monthStories = new ArrayList<MonthStories>();
		
		obj.forEach(item -> {
			MonthStories mStories = new MonthStories();
			mStories.setMonthNumber((int) item[1]);
			mStories.setMonthName(item[2].toString());
			mStories.setLatestStory(item[3].toString());
			mStories.setLatestStoryPicture(item[4].toString());
			monthStories.add(mStories);
		});
		
		return monthStories;
	}

}
