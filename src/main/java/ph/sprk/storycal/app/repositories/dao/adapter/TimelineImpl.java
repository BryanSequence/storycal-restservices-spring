package ph.sprk.storycal.app.repositories.dao.adapter;

import java.util.ArrayList;
import java.util.List;

import ph.sprk.storycal.app.helper.StoryCalDataBuilder;
import ph.sprk.storycal.app.mapper.YearStories;

public class TimelineImpl extends StoryCalDataBuilder<YearStories> {

	@Override
	public List<YearStories> constructData(List<Object[]> obj) {
		
		List<YearStories> yearStories = new ArrayList<YearStories>();
		
		obj.forEach(item -> {
			YearStories yrStories = new YearStories();
			yrStories.setYear((int) item[0]);
			yrStories.setLatestStory(item[1].toString());
			yrStories.setLatestStoryPicture(item[2].toString());
			yearStories.add(yrStories);
		});
		
		return yearStories;
	}

}
