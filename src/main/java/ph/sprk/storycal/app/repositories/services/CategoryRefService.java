package ph.sprk.storycal.app.repositories.services;

import java.util.List;

import ph.sprk.storycal.app.entities.StoryCategoryRef;

public interface CategoryRefService {
	
	public StoryCategoryRef saveCategoryRef(StoryCategoryRef storyCatRef);

	public StoryCategoryRef findByCategoryId(int categoryId);
	
	public StoryCategoryRef findByStoryCategory(String category);
	
	public List<StoryCategoryRef> findAll();
}
