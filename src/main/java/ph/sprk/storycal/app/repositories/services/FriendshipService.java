package ph.sprk.storycal.app.repositories.services;

import ph.sprk.storycal.app.entities.Friendship;

public interface FriendshipService {
	public Friendship save(Friendship friend);
	
	public Friendship findByUserId(int userId);
	
	public Friendship findByFollowingId(int followingId);
	
	public Friendship findByUserIdFollowing(int user_id,int following_id);
	
	public void deleteFollow(int follow_id);
}
