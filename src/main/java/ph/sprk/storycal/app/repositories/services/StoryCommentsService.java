package ph.sprk.storycal.app.repositories.services;

import ph.sprk.storycal.app.entities.StoryComments;

public interface StoryCommentsService {
	public StoryComments save(StoryComments storyComments);
	
	public StoryComments findByCommentId(int commentId);
}
