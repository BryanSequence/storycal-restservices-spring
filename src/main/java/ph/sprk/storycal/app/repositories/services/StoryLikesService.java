package ph.sprk.storycal.app.repositories.services;


import ph.sprk.storycal.app.entities.StoryLikes;

public interface StoryLikesService {
	
	public StoryLikes save(StoryLikes story);
	
	public StoryLikes findByLikeId(int like_id);
	
	public StoryLikes findByStoryIdUserId(int story_id,int user_id);
	
	public void deleteLike(int like_id);
}
