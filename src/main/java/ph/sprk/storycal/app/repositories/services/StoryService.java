package ph.sprk.storycal.app.repositories.services;


import java.util.Collection;

import ph.sprk.storycal.app.entities.UserStory;

public interface StoryService {
	public UserStory saveStory(UserStory userStory);
	
	public UserStory findByStoryId(int story_id);
	
	public UserStory findByUserId(int user_id);
	
	public Collection<UserStory> findAll();
}
