package ph.sprk.storycal.app.repositories.services;

import ph.sprk.storycal.app.entities.UserAccount;

public interface UserAccountService {
	public UserAccount saveUserAccount(UserAccount userAccount);
	
	public UserAccount findByUsername(String username);
}
