package ph.sprk.storycal.app.repositories.services;


import java.util.List;

import org.springframework.data.repository.query.Param;

import ph.sprk.storycal.app.entities.User;

public interface UserService {
	public User saveUser(User user);
	
	public User findUserByEmail(String email);
	
	public User findByUserId(int user_id);
	
	public List<User> findByName(@Param("name") String name);
	
	public User findByUserSecId(@Param("sec_id") int sec_id);
	
}
