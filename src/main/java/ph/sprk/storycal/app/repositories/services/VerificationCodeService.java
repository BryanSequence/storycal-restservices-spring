package ph.sprk.storycal.app.repositories.services;

import ph.sprk.storycal.app.entities.Verification;

public interface VerificationCodeService {
	public Verification saveVCode(Verification vcode);
	
	public Verification findByEmail(String email);
	
}
