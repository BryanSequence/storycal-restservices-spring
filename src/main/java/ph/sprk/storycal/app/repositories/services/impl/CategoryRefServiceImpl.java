package ph.sprk.storycal.app.repositories.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ph.sprk.storycal.app.entities.StoryCategoryRef;
import ph.sprk.storycal.app.repositories.CategoryRefRepository;
import ph.sprk.storycal.app.repositories.services.CategoryRefService;


@Service
public class CategoryRefServiceImpl implements CategoryRefService{

	@Autowired
	private CategoryRefRepository categoryRef;
	
	@Override
	public StoryCategoryRef saveCategoryRef(StoryCategoryRef storyCatRef) {
		return categoryRef.save(storyCatRef);
	}

	@Override
	public StoryCategoryRef findByCategoryId(int categoryId) {
		return categoryRef.findByCategoryId(categoryId);
	}

	@Override
	public StoryCategoryRef findByStoryCategory(String category) {
		return categoryRef.findByStoryCategory(category);
	}

	@Override
	public List<StoryCategoryRef> findAll() {
		return categoryRef.findAll();
	}

}
