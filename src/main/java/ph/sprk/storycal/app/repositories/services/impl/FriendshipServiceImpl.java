package ph.sprk.storycal.app.repositories.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ph.sprk.storycal.app.entities.Friendship;
import ph.sprk.storycal.app.repositories.FriendshipRepository;
import ph.sprk.storycal.app.repositories.services.FriendshipService;

@Service
public class FriendshipServiceImpl implements FriendshipService {

	@Autowired
	private FriendshipRepository friendRepository;
	
	@Override
	public Friendship save(Friendship friend) {
		return friendRepository.save(friend);
	}

	@Override
	public Friendship findByUserId(int userId) {
		return friendRepository.findByUserId(userId);
	}

	@Override
	public Friendship findByFollowingId(int followingId) {
		return friendRepository.findByFollowingId(followingId);
	}

	@Override
	public Friendship findByUserIdFollowing(int user_id, int following_id) {
		return friendRepository.findByUserIdFollowing(user_id, following_id);
	}

	@Override
	public void deleteFollow(int follow_id) {
		friendRepository.deleteById(follow_id);
	}

}
