package ph.sprk.storycal.app.repositories.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ph.sprk.storycal.app.entities.StoryComments;
import ph.sprk.storycal.app.repositories.StoryCommentsRepository;
import ph.sprk.storycal.app.repositories.services.StoryCommentsService;

@Service
public class StoryCommentsServiceImpl implements StoryCommentsService {
	
	@Autowired
	private StoryCommentsRepository commentRepository;

	@Override
	public StoryComments save(StoryComments storyComments) {
		return commentRepository.save(storyComments);
	}

	@Override
	public StoryComments findByCommentId(int commentId) {
		return commentRepository.findByCommentId(commentId);
	}
	

}
