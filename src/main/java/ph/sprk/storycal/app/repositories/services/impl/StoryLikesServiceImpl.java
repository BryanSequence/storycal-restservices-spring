package ph.sprk.storycal.app.repositories.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ph.sprk.storycal.app.entities.StoryLikes;
import ph.sprk.storycal.app.repositories.StoryLikesRepository;
import ph.sprk.storycal.app.repositories.services.StoryLikesService;

@Service
public class StoryLikesServiceImpl implements StoryLikesService {

	@Autowired
	private StoryLikesRepository storyLikesRepository;
	
	@Override
	public StoryLikes save(StoryLikes story) {
		return storyLikesRepository.save(story);
	}

	@Override
	public StoryLikes findByLikeId(int like_id) {
		return storyLikesRepository.findByLikeId(like_id);
	}

	@Override
	public StoryLikes findByStoryIdUserId(int story_id, int user_id) {
		return storyLikesRepository.findByStoryIdUserId(story_id, user_id);
	}

	@Override
	public void deleteLike(int like_id) {
		storyLikesRepository.deleteById(like_id);
	}

}
