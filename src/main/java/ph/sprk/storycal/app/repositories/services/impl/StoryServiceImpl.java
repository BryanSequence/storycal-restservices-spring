package ph.sprk.storycal.app.repositories.services.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ph.sprk.storycal.app.entities.UserStory;
import ph.sprk.storycal.app.repositories.StoryRepository;
import ph.sprk.storycal.app.repositories.services.StoryService;

@Service
public class StoryServiceImpl implements StoryService {

	@Autowired
	private StoryRepository storyRepository;
	
	@Override
	public UserStory saveStory(UserStory userStory) {
		return storyRepository.save(userStory);
	}

	@Override
	public UserStory findByStoryId(int story_id) {
		return storyRepository.findByStoryId(story_id);
	}

	@Override
	public UserStory findByUserId(int user_id) {
		return storyRepository.findByUserId(user_id);
	}

	@Override
	public Collection<UserStory> findAll() {
		return storyRepository.findAll();
	}

}
