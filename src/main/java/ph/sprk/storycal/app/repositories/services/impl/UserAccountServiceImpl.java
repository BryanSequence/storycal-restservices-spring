package ph.sprk.storycal.app.repositories.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ph.sprk.storycal.app.entities.UserAccount;
import ph.sprk.storycal.app.repositories.UserAccountRepository;
import ph.sprk.storycal.app.repositories.services.UserAccountService;

@Service
public class UserAccountServiceImpl implements UserAccountService {

	@Autowired
	private UserAccountRepository userAccountRepository;
	
	@Override
	public UserAccount saveUserAccount(UserAccount userAccount) {
		return userAccountRepository.save(userAccount);
	}

	@Override
	public UserAccount findByUsername(String username) {
		return userAccountRepository.findByUsername(username);
	}

}
