package ph.sprk.storycal.app.repositories.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ph.sprk.storycal.app.entities.User;
import ph.sprk.storycal.app.repositories.UserRepository;
import ph.sprk.storycal.app.repositories.services.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public User saveUser(User user) {
		return userRepository.save(user);
	}

	@Override
	public User findUserByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public User findByUserId(int user_id) {
		return userRepository.findByUserId(user_id);
	}

	@Override
	public User findByUserSecId(int sec_id) {
		return userRepository.findByUserSecId(sec_id);
	}

	@Override
	public List<User> findByName(String name) {
		return userRepository.findByName(name);
	}

}
