package ph.sprk.storycal.app.repositories.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ph.sprk.storycal.app.entities.Verification;
import ph.sprk.storycal.app.repositories.VerificationCodeRepository;
import ph.sprk.storycal.app.repositories.services.VerificationCodeService;


@Service
public class VerificationCodeImpl implements VerificationCodeService {

	@Autowired
	private VerificationCodeRepository verificationCodeRepository;
	
	
	@Override
	public Verification saveVCode(Verification vcode) {
		
		return verificationCodeRepository.save(vcode);
	}

	@Override
	public Verification findByEmail(String email) {
		return verificationCodeRepository.findByEmail(email);
	}


}
