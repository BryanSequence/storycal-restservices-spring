package ph.sprk.storycal.app.utils;

import java.util.HashMap;

import org.springframework.stereotype.Service;

@Service
public class GenericResponse {
	private HashMap<String, Object> response = new HashMap<String, Object>();
	
	public GenericResponse() {}
	
	public GenericResponse(boolean status) {
		this.setKeyValuePair("status", status);
	}
	
	public GenericResponse(String message) {
		this.setKeyValuePair("message", message);
	}
	
	public GenericResponse(boolean status, String message) {
		this.setKeyValuePair("status", status);
		this.setKeyValuePair("message", message);
	}
	
	public GenericResponse(boolean status, String message, Object object) {
		this.setKeyValuePair("status", status);
		this.setKeyValuePair("message", message);
		this.setKeyValuePair("data", object);
	}
	
	public void setKeyValuePair(String key, Object value) {
		response.put(key, value);
	}
	
	public Object getKeyValue(String key) {
		return response.get(key);
	}
	
	public Object getResponse() {
		return this.response;
	}
	
}
