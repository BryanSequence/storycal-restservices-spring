package ph.sprk.storycal.app.utils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import ph.sprk.storycal.app.mapper.Newsfeed;
import ph.sprk.storycal.app.mapper.PagedResult;
import ph.sprk.storycal.app.mapper.UserInfo;

public class NewsfeedUtils {
	
	private static final int DEFAULT_PAGE_SIZE = 10;

	private static final int DEFAULT_PAGE_NUMBER = 1;

	private int maxPageNumber;
	
	private int pageNumber;
	
	private int pageSize;
		
	private Newsfeed newsfeed;
	
	private UserInfo userInfo;
	
	@SuppressWarnings("unchecked")
	public PagedResult buildNewsFeed(EntityManager em, int userId, int pageNumber, int pageSize, boolean isPublic) {
		ArrayList<Newsfeed> listNewsfeed = new ArrayList<Newsfeed>();

		if (pageNumber == 0) 
			this.setPageNumber(DEFAULT_PAGE_NUMBER);
		else if ( pageSize == 0)
			this.setPageSize(DEFAULT_PAGE_SIZE);
		else {
			this.setPageNumber(pageNumber);
			this.setPageSize(pageSize);
		}
		
		this.setMaxPageNumber((this.countNewsfeedItem(em, userId, isPublic) / this.getPageSize()));
		
		if (isPublic) {
			
			StoredProcedureQuery query = em.createStoredProcedureQuery("newsfeed_public");
			
			query.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN);
			query.setParameter(1, userId);
			
//			query.setFirstResult((this.getPageNumber()-1) * this.getPageSize());
//			query.setMaxResults(this.getPageSize());
			query.execute();
			
			List<Object[]> listObj = query.getResultList();
			this.setMaxPageNumber(listObj.size()/pageSize);
			this.getPage(listObj, pageNumber, pageSize).forEach(o -> {
				newsfeed = new Newsfeed();
				userInfo = new UserInfo((int) o[0], o[1].toString(), o[2].toString());
				
				newsfeed.setUserInfo(userInfo);
				
				newsfeed.setStoryId(this.bigIntToInteger(o[3]));
				newsfeed.setTitle(o[4].toString());
				newsfeed.setStory(o[5].toString());
				newsfeed.setPhotoUrl(o[6].toString());
				newsfeed.setPrivacy(o[7].toString());
				newsfeed.setDatePosted(o[8].toString());
				newsfeed.setLikeCount(this.bigIntToInteger(o[9]));
				newsfeed.setCommentCount(this.bigIntToInteger(o[10]));
				newsfeed.setLiked(o[11].toString().equals("true") ? true : false);
				newsfeed.setFollowed(o[12].toString().equals("true") ? true : false);
				
				listNewsfeed.add(newsfeed);
			});
		} 
		else {
			StoredProcedureQuery query = em.createStoredProcedureQuery("newsfeed_private");
			
			query.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN);
			query.setParameter(1, userId);
			
//			query.setFirstResult((this.getPageNumber()-1) * this.getPageSize());
//			query.setMaxResults(this.getPageSize());
			query.execute();
			
			List<Object[]> listObj = query.getResultList();
			System.out.println("LIST SIZE: " +listObj.size());
			this.setMaxPageNumber(Math.round(listObj.size()/pageSize));

			this.getPage(listObj, pageNumber, pageSize).forEach(o -> {
				newsfeed = new Newsfeed();
				userInfo = new UserInfo((int) o[0], o[1].toString(), o[2].toString());
				
				newsfeed.setUserInfo(userInfo);
				
				newsfeed.setStoryId(this.bigIntToInteger(o[3]));
				newsfeed.setTitle(o[4].toString());
				newsfeed.setStory(o[5].toString());
				newsfeed.setPhotoUrl(o[6].toString());
				newsfeed.setPrivacy(o[7].toString());
				newsfeed.setDatePosted(o[8].toString());
				newsfeed.setLikeCount(this.bigIntToInteger(o[9]));
				newsfeed.setCommentCount(this.bigIntToInteger(o[10]));
				newsfeed.setLiked(o[11].toString().equals("true") ? true : false);
				newsfeed.setFollowed(o[12].toString().equals("true") ? true : false);
				
				listNewsfeed.add(newsfeed);
			});
		}
		
		return new PagedResult(listNewsfeed, this.getMaxPageNumber(), 0, this.getPageSize(), isPublic);
	}
	
	/**
	 * Helper Methods
	 */
	
	private <T> List<T> getPage(List<T> sourceList, int page, int pageSize) {
	    if(pageSize <= 0 || page <= 0) {
	        throw new IllegalArgumentException("invalid page size: " + pageSize);
	    }

	    int fromIndex = (page - 1) * pageSize;
	    if(sourceList == null || sourceList.size() < fromIndex){
	        return Collections.emptyList();
	    }

	    return sourceList.subList(fromIndex, Math.min(fromIndex + pageSize, sourceList.size()));
	}
	
	private int countNewsfeedItem(EntityManager em, int userId, boolean isPublic) {
		String storedProc = null;
		int total_count = 0;
		
		if (isPublic) {
			storedProc = "count_newsfeed_public";
			StoredProcedureQuery query = em.createStoredProcedureQuery(storedProc);
				query.execute();
				total_count = this.bigIntToInteger(query.getSingleResult());
		}
		else {
			storedProc = "count_newsfeed_private";
			
			StoredProcedureQuery query = em.createStoredProcedureQuery(storedProc);
				query.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN);
				query.setParameter(1, userId);
				query.execute();
				total_count = this.bigIntToInteger(query.getSingleResult());
		}
				
		return total_count;
	}
	
	
	private int  bigIntToInteger(Object number) {
		return new BigInteger(number.toString()).intValue();
	}

	
	/**
	 * Setter and Getter
	 */
	
	public int getMaxPageNumber() {
		return maxPageNumber;
	}

	public void setMaxPageNumber(int maxPageSize) {
		this.maxPageNumber = maxPageSize;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

}
