package ph.sprk.storycal.app.utils;

import java.io.Serializable;
import java.security.SecureRandom;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Service;

@Service
public class PasswordUtils implements Serializable {

	private static final long serialVersionUID = 8874940660777362064L;
	// The higher the number of iterations the more 
    // expensive computing the hash is for us and
    // also for an attacker.
    private final int iterations = 20*1000;
    private final int saltLen = 32;
    private final int desiredKeyLen = 256;

    
    /**
     * Computes a salted PBKDF2 hash of given plaintext password
     * suitable for storing in a database 
     * 
     * NOTE: Empty passwords are not supported.
     * 
     * 
     * @param password
     * @return
     * @throws Exception
     */
    public String getSaltedHash(String password) throws Exception {
        byte[] salt = SecureRandom.getInstance("SHA1PRNG").generateSeed(saltLen);
        // store the salt with the password
        return Base64.encodeBase64String(salt) + "$" + hash(password, salt);
    }

    
    /**
     * Check whether given plaintext password corresponds to a
     * stored salted hash of the password
     * 
     * @param password
     * @param stored
     * @return
     * @throws Exception
     */
    public boolean check(String password, String stored) throws Exception{
        String[] saltAndPass = stored.split("\\$");
        if (saltAndPass.length != 2) {
            throw new IllegalStateException("The stored password have the form 'salt$hash'");
        }
        String hashOfInput = hash(password, Base64.decodeBase64(saltAndPass[0]));
        return hashOfInput.equals(saltAndPass[1]);
    }

    // using PBKDF2 from Sun, an alternative is https://github.com/wg/scrypt
    // cf. http://www.unlimitednovelty.com/2012/03/dont-use-bcrypt.html
    private String hash(String password, byte[] salt) throws Exception {
        if (password == null || password.length() == 0)
            throw new IllegalArgumentException("Empty passwords are not supported.");
        SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        SecretKey key = f.generateSecret(new PBEKeySpec(
            password.toCharArray(), salt, iterations, desiredKeyLen)
        );
        return Base64.encodeBase64String(key.getEncoded());
    }

}
