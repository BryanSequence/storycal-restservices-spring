package ph.sprk.storycal.app.utils;

import java.util.Random;
import org.springframework.stereotype.Service;

@Service
public class VerificationCodeGenerator {
	
	/**
	 * 
	 * version 1.0 code generation
	 * 
	 * @param len - length of verification code
	 * @return
	 */
	public String verificationCode(int len) {
		String numbers = "0123456789";
		Random rand = new Random();
		
		char[] vcode = new char[len];
		
		for (int i = 0; i < len; i++) {
			vcode[i] = numbers.charAt(rand.nextInt(numbers.length()));
		}
		return String.valueOf(vcode);
	}
}
